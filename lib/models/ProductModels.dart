import 'package:gamegarage/gamegarage.dart';

class ProductModels extends Serializable{
  ProductModels({this.title, this.author, this.desc, this.releaseDate, this.imageUrl});

  String title;
  String author;
  String desc;
  String releaseDate;
  String imageUrl;

  @override
  Map<String, dynamic> asMap() => {
    "title": title,
    "author": author,
    "desc": desc,
    "release_date": releaseDate,
    "image_url": imageUrl
  };

  @override
  void readFromMap(Map<String, dynamic> object) {
    title = object['title'] as String;
    author = object['author'] as String;
    desc = object['desc'] as String;
    releaseDate = object['release_date'] as String;
    imageUrl = object['image_url'] as String;
  }

}