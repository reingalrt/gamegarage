import 'dart:developer';

import 'package:gamegarage/gamegarage.dart';
import 'package:gamegarage/models/ProductModels.dart';

class ProductController extends ResourceController {

  List games = [
    {
      'title': 'Dead Red Redemption',
      'author': 'BintangBatu studio',
      'desc': 'A game about dead red',
      'release_date': '2016',
      'image_url': 'https://cdn.cdkeys.com/500x706/media/catalog/product/r/e/red-dead-redemption-2-pc-cd-keys-rockstar.jpg'
    },
    {
      'title': 'Mati Kesasar',
      'author': 'Parto production',
      'desc': 'Game tentang orang yang mati kesasar',
      'release_date': '2016',
      'image_url': 'https://www.indiewire.com/wp-content/uploads/2019/10/Death-Stranding-logo.jpg'
    },
    {
      'title': 'Legenda si zelda',
      'author': 'Nenendong',
      'desc': 'Game tentang anak yang masuk ke goa ketemu kakek jav ngasih pedang pedangan',
      'release_date': '2016',
      'image_url': 'https://zelda.com/breath-of-the-wild/assets/icons/BOTW-Share_icon.jpg'
    }
  ];
  

  @Operation.get()
  Future<Response> getAll() async => Response.ok(games);

  @Operation.get('index')
  Future<Response> getProduct(@Bind.path("index") int idx) async{
    if(idx < 0 || idx > games.length - 1){
      return Response.notFound(body: 'Game does not exist, maybe the owner deleted them');
    }

    return Response.ok(games[idx]);
  }

  @Operation.post()
  Future<Response> postProduct(@Bind.body() ProductModels game) async { 
    games.add(game);
    print(games.toString());
    return Response.ok(game);
  }

  @Operation.put('index')
  Future<Response> updateProduct(@Bind.path("index") int idx) async{
    if(idx < 0 || idx > games.length - 1){
      return Response.notFound(body: 'Game does not exist, maybe the owner deleted them');
    }

    var body = request.body.as<Map>();
    for(var i = 0; i < games.length; i++){
      if(i == idx){
        games[i]['title'] = body['title'];
        games[i]['author'] = body['author'];
        games[i]['desc'] = body['desc'];
        games[i]['release_date'] = body['release_date'];
        games[i]['image_url'] = body['image_url'];
      }
    }
    return Response.ok(body);
  }

  @Operation.delete('index')
  Future<Response> deleteProduct(@Bind.path("index") int idx) async {
    if(idx < 0 || idx > games.length - 1){
      return Response.notFound(body: 'Game does not exist, maybe the owner deleted them');
    }

    games.removeAt(idx);
    return Response.ok('Your game deleted successfuly');
  }
}
